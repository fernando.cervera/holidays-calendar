import fetch from 'isomorphic-unfetch'

const BASE_URL = 'https://date.nager.at/api/v2'

export default async (req, res) => {
  const query = req.query.slug.join('/')
  const response = await fetch(`${BASE_URL}/${query}`)
  const jsonResponse = await response.json()
  res.send(jsonResponse)
}
